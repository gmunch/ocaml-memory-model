/* Copyright (c) 2021, 2022, Guillaume Munch-Maccagnoni
   SPDX-License-Identifier: LGPL-3.0-only WITH LGPL-3.0-linking-exception
*/

use core::marker::PhantomData;

/** This is a linear capability that represents possession of an
 * OCaml _domain lock_. An OCaml domain is a collection of system
 * threads, running sequentially by holding the domain lock in
 * turn. In particular, the `DomainLock` capability cannot be sent
 * to or shared with arbitrary threads. */
pub struct DomainLock {
    /* Make it !Send and !Sync by using core::cell::Ref */
    _marker: PhantomData<core::cell::Ref<'static, ()>>,
}

static mut DUMMY: DomainLock = DomainLock {
    _marker: PhantomData,
};

impl DomainLock {
    /** `create` and `create_mut` assert that the domain lock is
     * held and no other DomainLock capability presently
     * represents its ownership. We leave it to third-party
     * libraries to determine when the domain lock is held. */
    #[inline]
    pub const unsafe fn create() -> DomainLock {
        DomainLock {
            _marker: PhantomData,
        }
    }

    #[inline]
    pub unsafe fn create_mut() -> &'static mut DomainLock {
        &mut DUMMY
    }
}

/** We ensure panic-safety for the OCaml runtime (TODO: explain more) */
impl UnwindSafe for DomainLock {}
impl RefUnwindSafe for DomainLock {}
