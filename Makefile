# Entry points

.PHONY: entry
entry:
	@echo "make test"
	@echo "make clean"

.PHONY: test
test:
	cargo build --features "link-ocaml-runtime-and-dummy-program" --verbose && \
	cargo test --features "link-ocaml-runtime-and-dummy-program" --verbose

.PHONY: clean
clean:
	cargo clean
