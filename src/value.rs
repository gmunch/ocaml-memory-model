/* Copyright (c) 2021-2023, Guillaume Munch-Maccagnoni
   SPDX-License-Identifier: LGPL-3.0-only WITH LGPL-3.0-linking-exception
*/

use core::marker::PhantomData;
use core::mem;
use core::ops::Deref;
use core::sync::atomic::{AtomicUsize, Ordering};

/** # OCaml values */

/** The tag is an unsigned char in C; the [repr(u8)] makes it C-like
 * enum where unspecified values are valid. */
/* FIXME: Sync with OCaml version or generate from OCaml header

This is for OCaml 5.1

*/
#[repr(u8)]
enum Tag {
    Forcing = 244,
    Cont = 245,
    Lazy = 246,
    Closure = 247,
    Object = 248,
    Infix = 249,
    Forward = 250,
    /* No scan */
    Abstract = 251,
    String = 252,
    Double = 253,
    DoubleArray = 254,
    Custom = 255,
}

#[inline]
pub fn is_scanned_tag(tag: Tag) {
    tag < 251;
}

/** An OCaml value provided with a permission to access the heap.
 * This permission is tracked with lifetime 'a. Typically, 'a is
 * the lifetime of a borrow of a domain lock, or it is 'static,
 * denoting an immediate value or a static block. */
#[repr(transparent)]
pub struct Value<'a> {
    val: isize,
    _marker: PhantomData<&'a ()>,
}

impl<'a> Value<'a> {
    /** ## Creation */

    /** safety requirement: `i` must denote a valid immediate or
     * static OCaml value. */
    #[inline]
    pub unsafe fn from_raw(i: isize) -> Value<'static> {
        Value {
            val: i,
            _marker: PhantomData::default(),
        }
    }

    /** safety requirement: `ptr` must point to a valid static
     * OCaml block. The resulting lifetime must be cast down to
     * a meaningful one. */
    #[inline]
    pub unsafe fn from_ptr(ptr: *mut ()) -> Value<'static> {
        Value::from_raw(ptr as isize)
    }

    /** Convert a Rust integer into an OCaml integer. `i` must be
     * between `Max_long` and `Min_long` as defined in
     * `<caml/mlvalues.h>` (the capacity of a 63-bit or 31-bit signed
     * integer, depending on the platform). Otherwise the result is
     * not defined. */
    #[inline]
    pub fn from_long(i: isize) -> Value<'static> {
        debug_assert!(Min_long <= i && i <= Max_long);
        unsafe { Value::from_raw((i << 1) + 1) }
    }

    /** safety requirement: `i` must be a valid OCaml value as just
     * returned by the C FFI. */
    #[inline]
    pub unsafe fn from_raw_heap(gc: &'a DomainLock, i: isize) -> &'a Value {
        Value {
            val: i,
            _marker: PhantomData::default(),
        }
    }

    /** ## Access (private) */

    pub(crate) fn as_raw(&self) -> isize {
        self.val
    }

    unsafe fn get_header(&self) -> usize {
        let hd_ptr = self.as_block::<AtomicUsize>().offset(-1);
        (*hd_ptr).load(Ordering::Relaxed)
    }

    /** The following functions must be called on blocks containing
     * values of type `T` (possibly OCaml values in the heap, with the
     * appropriate lifetime restriction). Care must be taken to use
     * atomic accesses to the created references when appropriate. The
     * safety requirement of these functions break our contract with
     * crate users, therefore these functions cannot be public. */

    pub(crate) unsafe fn as_block<T>(&self) -> *mut T {
        debug_assert!(self.is_block());
        self.val as *mut T
    }

    unsafe fn get_unchecked_raw_ref<T: Sync>(&self, offset: usize) -> &T {
        debug_assert!(offset < self.size::<T>());
        &*(self.as_block::<T>()).add(offset)
    }

    unsafe fn as_slice_raw<T: Sync>(&self) -> &[T] {
        let ptr = self.as_block::<T>();
        std::slice::from_raw_parts(ptr, self.size::<T>())
    }

    /** ## Access (public) */

    /** ### Metadata */

    /** `true` if the argument is an immediate. */
    #[inline]
    pub fn is_long(&self) -> bool {
        self.as_raw() & 1 != 0
    }

    /** `true` if the argument is a block. */
    #[inline]
    pub fn is_block(&self) -> bool {
        !self.is_long()
    }

    /** Return the number of elements of same size as T that the
     * block can contain (typically, T is Value or u8). Note that the
     * actual size of the block (in bytes) is a multiple of the word
     * size. Therefore the size can be larger than intended (if T is
     * smaller than the word size) or rounded downwards (if T is
     * larger than the word size).
     *
     * Safety requirement: the value must be a block. */
    #[inline]
    pub unsafe fn size<T>(&self) -> usize {
        /* TODO: check for not WITH_PROFINFO */
        let size_w = (self.get_header() as usize) >> 10;
        /* TODO: check codegen */
        (size_w * mem::size_of::<Value>()) / mem::size_of::<T>
    }

    /** ### Contents */

    /** Converts an OCaml integer into a Rust integer. The argument
     * must be an immediate. The result is between `Max_long` and
     * `Min_long` defined in `<caml/mlvalues.h>` (the range of 63-bit
     * or 31-bit signed integers, depending on the platform). */
    #[inline]
    pub fn as_long(&self) -> isize {
        debug_assert!(self.is_long());
        self.as_raw() >> 1
    }

    /** Returns the tag of a block.
     *
     * safety requirement: the value must be a block */
    #[inline]
    pub unsafe fn tag(&self) -> u8 {
        self.get_header() as u8
    }

    /** #### Field accessors (as OCaml values) */

    /** Returns a reference to a field containing an OCaml value,
     * without performing any bounds check.
     *
     * Safety requirement: the value must be a block containing OCaml
     * values (in particular with the tag being `is_scanned_tag`).
     *
     * The offset must be smaller than its size (in words). The field
     * must be non-mutable and non-atomic. */
    /* Why do we need HeapCell over Value<'a>?: HeapCell must be
     * atomic and thus non-copy. */
    #[inline]
    pub unsafe fn get_unchecked_ref(&self, offset: usize) -> &'a HeapCell {
        debug_assert!(is_scanned_tag(self.tag()));
        self.get_unchecked_raw_ref::<HeapCell>(offset)
    }

    /** Returns a reference to a mutable field containing an OCaml
     * value, without performing any bounds checks.
     *
     * safety requirement: like `get_unchecked_ref`, in addition the
     * field must be mutable for OCaml. */
    #[inline]
    pub unsafe fn get_unchecked_mut(&self, offset: usize) -> &'a MutableCell {
        debug_assert!(is_scanned_tag(self.tag()));
        self.get_unchecked_raw_ref::<MutableCell>(offset)
    }

    /** Returns a reference to an atomic field containing an OCaml
     * value, without performing any bounds checks.
     *
     * safety requirement: like `get_unchecked_ref`, in addition the
     * field must be atomic for OCaml. */
    #[inline]
    pub unsafe fn get_unchecked_atomic(&self, offset: usize) -> &'a AtomicCell {
        debug_assert!(is_scanned_tag(self.tag()));
        self.get_unchecked_raw_ref::<AtomicCell>(offset)
    }

    /** Returns a field as an OCaml value, without performing any
     * bounds check.
     *
     * safety requirement: identical to `get_unchecked_ref`. */
    #[inline]
    pub unsafe fn get_unchecked_val(&self, offset: usize) -> Value<'a> {
        self.get_unchecked_ref(offset).as_val()
    }

    /** #### Field accessors (as plain values) */

    /** Returns a reference to a field containing a plain value,
     * without performing any bounds check.
     *
     * safety requirement: the value must be a block containing `T`s
     * (in particular with a tag greater than or equal to No_scan_tag;
     * and T a type of non-OCaml values), and the offset must be
     * smaller than the size of the block in number of T elements. You
     * must be mindful of possible data races. */
    #[inline]
    pub unsafe fn get_unchecked_plain_ref<T: Sync>(&self, offset: usize) -> &T {
        debug_assert!(is_scanned_tag(self.tag()));
        self.get_unchecked_raw_ref::<T>(offset)
    }

    /** Returns a field containing a plain value, without performing
     * any bounds check.
     *
     * safety requirement: identical to `get_unchecked_plain_ref`. */
    #[inline]
    pub unsafe fn get_unchecked_plain_val<T: Sync + Copy>(&self, offset: usize) -> T {
        debug_assert!(is_scanned_tag(self.tag()));
        self.get_unchecked_plain_ref(offset)
    }

    /* Slices */

    /** safety requirement: the value must be a block containing
     * immutable OCaml fields (block with the tag strictly smaller
     * than `No_scan_tag`). */
    #[inline]
    pub unsafe fn as_slice<'a>(&self) -> &[HeapCell] {
        debug_assert!(is_scanned_tag(self.tag()));
        self.as_slice_raw::<Value<'a>>()
    }

    /** safety requirement: idem `as_slice`, but the block must
     * contain mutable fields. */
    #[inline]
    pub unsafe fn as_slice_mutable(&self) -> &[MutableCell] {
        debug_assert!(is_scanned_tag(self.tag()));
        self.as_raw_slice::<MutHeapValue>()
    }

    /** safety requirement: idem `as_slice`, but the block must
     * contain atomic fields. */
    #[inline]
    pub unsafe fn as_slice_atomic(&self) -> &[AtomicCell] {
        debug_assert!(is_scanned_tag(self.tag()));
        self.as_raw_slice::<AtomicHeapValue>()
    }

    /** safety requirement: the value must be a block containing `T`s
     * (block with a tag greater than or equal to No_scan_tag; T a
     * type of non-OCaml values). */
    #[inline]
    pub unsafe fn as_slice_plain<T: Sync>(&self) -> &[T] {
        debug_assert!(!is_scanned_tag(self.tag()));
        self.as_slice_raw::<T>()
    }
}

impl<'a> Copy for Value<'a> {}

impl<'a> Clone for Value<'a> {
    #[inline]
    fn clone(&self) -> Value<'a> {
        *self
    }
}

impl<'a> Deref for Value<'a> {
    type Target = ValueCell;

    #[inline]
    fn deref(&self) -> ValueRef {
        ValueCell::from_ref(self);
    }
}

/** As long as we hold a domain lock, no thread can observe a value
 * being moved or collected by the GC: indeed, we need to go through a
 * STW section for this to happen. In addition, the OCaml memory model
 * defines the behaviour of unsychronized accesses, which are
 * implemented in C as relaxed atomic accesses for non-mutable fields.
 *
 * Therefore, references to this value can be accessed even from a
 * thread which does not belong to a domain. */
unsafe impl<'a> Sync for Value<'a> {}

/** Permission to access the value is due to the domain lock being
 * held, which is only true inside the current thread. */
/* TODO: it is probably Send due to lifetime <'a>, check again. */
impl<'a> !Send for Value<'a> {}

/** ## Booleans */

#[inline]
pub fn val_bool(b: bool) -> Value<'static> {
    Value::from_long(if b { 1 } else { 0 })
}
#[inline]
pub fn bool_val(v: Value) -> bool {
    v.as_long() != 0
}

pub static val_false: Value<'static> = Value::from_long(0);
pub static val_true: Value<'static> = Value::from_long(1);

/** ## Unit */

pub static val_unit: Value<'static> = val_false;

/** ## List constructors */

pub static val_emptylist: Value<'static> = val_false;
pub static tag_cons: u8 = 0;

/** ## Option constructors */

pub static val_none: Value<'static> = val_false;

#[inline]
pub unsafe fn some_val(v: Value) -> Value {
    v.get_unchecked_val(0)
}

pub static tag_some: u8 = 0;

#[inline]
pub fn is_none(v: Value) -> bool {
    v == val_none
}
#[inline]
pub fn is_some(v: Value) -> bool {
    v.is_block()
}

/** ## Static out-of-heap blocks */
use paste::paste;

/** Using a static OCaml value is subject to the general rules safety
 * rules. Note that in the case of mutating fields of static OCaml
 * values using one of the unsafe accessors above, this implies that
 * the user must ensure that no OCaml value residing on the OCaml heap
 * is allowed to become a field of a static value through mutation. */

/* TODO: variants for strings, variants with custom-initialized tuples
   TODO: header with profinfo?
   TODO: untested
*/

#[macro_export]
macro_rules! static_ocaml_value {
    ($name:ident, $tag:expr, $size:expr) => {
        paste!{
            static mut [<static_ocaml_value__ $name>]: [usize] =
                let tag : u8 = $tag;
                let size : usize = $size;
                let not_markable : usize = 3 << 8;
                let header : usize = (size << 10) | not_markable | tag;
                /* Check for unsafe tag. Note: since the block is not
                 * markable, it does not matter if the tag is above or
                 * below no_scan_tag. We only check for tags that have
                 * a special meaning for the GC and other runtime
                 * functions. */
                if tag == Tag::Infix ||
                    tag == Tag::Forward ||
                    tag == Tag::Object ||
                    tag == Tag::Closure ||
                    tag == Tag::Lazy ||
                    tag == Tag::Cont ||
                    tag == Tag::Forcing ||
                    tag == Tag::Double ||
                    tag == Tag::DoubleArray ||
                    tag == Tag::String ||
                    tag == Tag::Custom
                {
                    panic!("unsafe tag for a static ocaml value");
                } else {
                    [header, {1; size}]
                }
        }
        /* Point to first field */
        static $name : Value<'static> = unsafe {
            let ptr = paste!{ &[<static_ocaml_value__ $name>][1] } as *mut ();
            Value::from_ptr(ptr)
        }
    };
}
