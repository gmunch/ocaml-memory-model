use crate::ocaml::{DomainLock, Value, ValueRef};
use crate::{boxroot_teardown, Boxroot};
use std::ffi::CString;

extern "C" {
    pub fn caml_startup(argv: *const *const i8);
    pub fn caml_shutdown();
}

#[test]
fn it_works() {
    let exe = CString::new("ocaml-boxroot").unwrap();
    let argv = [exe.as_ptr(), 0 as *const i8];

    unsafe { caml_startup(argv.as_ptr()) };
    let gc = unsafe { DomainLock::create() };

    {
        let mut root = Boxroot::new(Value::from_long(0));
        let v1: Value = root.as_val(&gc);

        root.keep(&gc, Value::from_long(1));
        let v2: ValueRef = &root;

        assert_eq!(v2.as_val(&gc).as_long(), 1);
        assert_eq!(v1.as_long(), 0);
        // Drop root
    }

    unsafe {
        caml_shutdown();
        boxroot_teardown();
    }
}
