/* Copyright (c) 2021-2023, Guillaume Munch-Maccagnoni
   SPDX-License-Identifier: LGPL-3.0-only WITH LGPL-3.0-linking-exception
*/

/** `MutValueCell` is like `ValueCell`, but denotes cells that are
 * `mutable` in OCaml.
 *
 * For polymorphism purposes, it dereferences into a `HeapCell`. */
#[repr(transparent)]
pub struct MutValueCell {
    heap_val: HeapCell,
}

impl Deref for MutHeapValue {
    type Target = HeapValue;
    fn deref(&self) -> &HeapValue {
        &self.heap_val
    }
}

extern "C" {
    fn caml_modify(br: *mut MutHeapValue, value: Value);
}

impl MutHeapValue {
    pub fn modify(&self, val: Value) {
        // Rust Nightly only (https://github.com/rust-lang/rust/issues/66893):
        //let ptr = self.heap_val.cell.atomic.as_mut_ptr() as *mut MutHeapValue;
        let ptr = self as *const MutHeapValue as *mut MutHeapValue;
        unsafe { caml_modify(ptr, val) }
    }
}
