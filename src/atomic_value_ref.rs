/* Copyright (c) 2021-2023, Guillaume Munch-Maccagnoni
   SPDX-License-Identifier: LGPL-3.0-only WITH LGPL-3.0-linking-exception
*/

/** `AtomicHeapValue` is like `MutHeapValue` but for an atomic
 * OCaml cell. This type implements the OCaml memory model for
 * atomic locations in OCaml's heap.
 *
 * It cannot be converted into a `HeapValue`: accessing with a
 * relaxed load is not (yet) defined in the OCaml memory model.
 * Therefore this type does not implement `Deref`. To operate with
 * its contents, first read it with `AtomicHeapValue::load`. */
#[repr(transparent)]
pub struct AtomicHeapValue {
    atomic_val: AtomicIsize,
}

extern "C" {
    /*
    I remember that some review was necessary before calling these
    from C code (some fence optimization done in OCaml maybe?).

    fn caml_atomic_load;
    fn caml_atomic_exchange;
    fn caml_atomic_cas;
    fn caml_atomic_fetch_add;
     */
}

impl AtomicHeapValue {
    pub fn load<'a>(&'a self) -> Value<'a> {
        panic!("caml_atomic_load unavailable")
    }

    pub fn exchange<'a>(&'a self, _val: Value) -> Value<'a> {
        panic!("caml_atomic_exchange unavailable")
    }

    pub fn compare_and_set<'a>(&'a self, _old: Value, _new: Value) -> Value<'a> {
        panic!("caml_atomic_cas unavailable")
    }

    pub fn fetch_and_add<'a>(&'a self) -> Value<'a> {
        panic!("caml_atomic_fetch_add unavailable")
    }
}
