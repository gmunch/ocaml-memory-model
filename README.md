# This is work in progress and does not compile yet

# OCaml memory model for Rust: resource- and memory-safe interface to OCaml

This library provides a low-level resource- and memory-safe interface
to OCaml. Its goal is to serve as a common entry point for
higher-level OCaml-Rust interfaces.

In order to ensure safety, an OCaml API in Rust must take care of the
following:

1. Enforce the OCaml memory model and representation
2. Enforce the GC invariants
3. Enforce type-safety (describing the shape of values by ascribing
   types to expressions)

The goal of this library is to solve 1. and 2. in the most generic
manner, and to let client libraries explore their own way of enforcing
3.

Thus, many functions in this crate are marked `unsafe`, but this comes
with the following contract:
- the conditions to safely use these functions are always documented,
  and
- these conditions are never about 1. and 2. (this crate is safe as
  far as 1. and 2. are concerned).

This library uses
[Boxroot](https://gitlab.com/ocaml-rust/ocaml-boxroot/) for rooting
values in order to enforce the GC invariants.

## DomainLock

The DomainLock capability denotes ownership of the OCaml domain lock.

## Calling convention

This library recognizes 3 kinds of functions that can manipulate OCaml
values to different degrees according to how they depend on the
DomainLock capability:

1. **0**: No runtime access (`fn f(...)`). The function can access the
   heap via supplied arguments, but it cannot call runtime functions
   nor can dereference roots.

2. **RO**: Read-only runtime access (`fn f(gc: &DomainLock, ...)`). The
   function can call runtime functions which do not trigger a GC, and
   it can dereference roots.

3. **RW**: Read-write runtime access (`fn f(gc: &mut DomainLock, ...)`).
   The function can call allocation functions and release and re-acquire
   the runtime lock.

|                                  | 0   | RO  | RW                      |
|----------------------------------|-----|-----|-------------------------|
| Types with value semantics       |     |     |                         |
| * `Value<'a>`                    | Yes | Yes | Only if 'static         |
| * `ValueExn<'a>`                 | Yes | Yes | No*                     |
| * `ValueUnshared<'a>`            | Yes | Yes | No*                     |
| * `ValueUnsharedSmall<'a>`       | Yes | Yes | No*                     |
| Types with reference semantics   |     |     |                         |
| * `&'a ValueCell = ValueRef<'a>` | No* | Yes | Yes (for rooted values) |
| * `&'a HeapValue`                | Yes | Yes | No*                     |
| * `&'a MutHeapValue`             | Yes | Yes | No*                     |
| * `&'a AtomicHeapValue`          | Yes | Yes | No*                     |
| Rooted values                    |     |     |                         |
| * `Boxroot`                      | No* | Yes | Yes                     |

*: Not in a useful way


## Running tests

The `link-ocaml-runtime-and-dummy-program` feature needs to be enabled when running tests:

    cargo test --features "link-ocaml-runtime-and-dummy-program"

## Feature .flags

### `no-std`

### `exception-safe`
