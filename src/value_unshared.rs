/* Copyright (c) 2021, 2022, Guillaume Munch-Maccagnoni
   SPDX-License-Identifier: LGPL-3.0-only WITH LGPL-3.0-linking-exception
*/

use crate::value::Value;
use core::marker::PhantomData;
use core::mem::size_of;

/** Phantom type to distinguish young from possibly-old values. Young
 * values can be initialized directly, old values must go through
 * caml_initialize. */
struct MaybeOld;
struct Young;

#[repr(transparent)]
pub struct ValueUnsharedBase<'a, T> {
    val: Value<'a>,
    _nocopy: PhantomData<AtomicIsize>, // impl !Copy
    _age: T,
}

/** A freshly-allocated value. Its contents can be initialized
 * efficiently before the value is shared.
 *
 * It is not copiable, and it cannot be inserted into the heap without
 * being consumed. */
pub type ValueUnshared<'a> = ValueUnsharedBase<'a, MaybeOld>;

/** Like ValueUnshared, but with the added knowledge that the value is
 * inside the minor heap. Its contents can be initialized even more
 * efficiently than a ValueUnshared before the value is shared. */
pub type ValueUnsharedYoung<'a> = ValueUnsharedBase<'a, Young>;

impl<'a, T> ValueUnshared<'a, T> {
    pub fn as_val(self) -> Value<'a> {
        self.val
    }

    pub(crate) unsafe fn set_unchecked_raw<T: Sync + Copy>(&mut self, offset: usize, val: T) {
        /*debug_assert!(offset < (size * ));*/
        let ptr = self.val.as_block::<T>().add(offset);
        *ptr = val;
    }

    pub(crate) unsafe fn as_mut_slice_raw<T: Sync + Copy>(&mut self) -> &mut [T] {
        let ptr = self.val.as_block::<T>();
        let size = self.val.size() * size_of::<Value>() / size_of::<T>();
        std::slice::from_raw_parts_mut(ptr, size)
    }

    /** safety requirement: the value must be a block containing `T`s
     * (with a tag greater than or equal to No_scan_tag; T a type of
     * non-OCaml values), and the offset must be smaller than the size
     * of the block in number of T elements. */
    #[inline]
    pub unsafe fn initialize_unchecked_plain<T: Sync + Copy>(&mut self, offset: usize, val: T) {
        debug_assert!(self.val.tag() >= No_scan_tag);
        self.set_unchecked_raw(ptr, val);
    }

    /** safety requirement: the value must be a block containing `T`s
     * (with a tag greater than or equal to No_scan_tag; T a type of
     * non-OCaml values). */
    /** safety requirement: the value must be a block containing
     * `T`s. Note that if `T` is smaller than `usize`, then the
     * size is a multiple of `sizeof<usize>/sizeof<T>` (see for
     * instance how a special encoding of string length addresses
     * this problem for OCaml strings). Therefore one must also be
     * sure that the block is correctly initialized until the
     * end. */
    #[inline]
    pub unsafe fn as_slice_plain_mut<T: Sync + Copy>(&mut self) -> &mut [T] {
        debug_assert!(self.val.tag() >= No_scan_tag);
        self.as_mut_slice_raw::<T>();
    }
}

extern "C" {
    fn caml_initialize(br: *mut Value, value: Value);
}

impl<'a> ValueUnshared<'a, MaybeOld> {
    /** safety requirement: the value must be a block containing OCaml
     * values (e.g. with the tag strictly smaller than `No_scan_tag`).
     * The offset must be smaller than its size (in words). */
    #[inline]
    pub unsafe fn initialize_unchecked(&mut self, offset: usize, val: Value) {
        debug_assert!(self.val.tag() < No_scan_tag);
        let ptr = self.val.as_block::<Value<'a>>().add(offset);
        caml_initialize(ptr, val)
    }
}

impl<'a> ValueUnshared<'a, Young> {
    /** safety requirement: the value must be a block containing OCaml
     * values (e.g. with the tag strictly smaller than `No_scan_tag`).
     * The offset must be smaller than its size (in words). */
    #[inline]
    pub unsafe fn initialize_unchecked(&mut self, offset: usize, val: Value) {
        debug_assert!(self.val.tag() < No_scan_tag);
        self.set_unchecked_raw(offset, val)
    }

    /** safety requirement: the value must be a block containing OCaml
     * values (e.g. with the tag strictly smaller than `No_scan_tag`).
     * TODO: review usability */
    #[inline]
    pub unsafe fn as_slice_mut(&mut self, gc: &'a DomainLock) -> &mut [Value<'a>] {
        debug_assert!(self.val.tag() < No_scan_tag);
        self.as_mut_slice_raw::<Value<'a>>();
    }
}
