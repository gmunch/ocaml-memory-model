/* Copyright (c) 2021, 2022, Guillaume Munch-Maccagnoni
   SPDX-License-Identifier: LGPL-3.0-only WITH LGPL-3.0-linking-exception
*/

use crate::value::Value;
use core::marker::PhantomData;
use core::option::Option;
use core::result::Result;
use core::string::String;

/*#![features(no_std)]
//Requires Rust 1.64*/
use core::ffi::{c_char, CStr};
/*#![not(features(no_std))]
use std::ffi::CStr, std::os::raw::{c_char};*/

/** A type with value semantics which denotes a possibly-encoded
 * exception, typically returned by the functions of the OCaml runtime
 * that end in `_exn`.
 *
 * If a function from the C runtime returns such an exception, then
 * this exception is currently being raised. It can denote a serious
 * error and should not be discarded without a sound justification. In
 * general, it should be re-raised as a panic. As an example, to
 * re-raise the exception itself as a panic, it can be wrapped in a
 * `Boxroot` together with its OCaml backtrace.
 *
 * In addition, pattern-matching on the value of an exception is
 * difficult from Rust. Thus, in order to represent recoverable
 * errors, it is recommended to use types such as OCaml's `Result.t`,
 * which is more easily convertible to Rust's `Result` type. */
#[repr(transparent)]
pub struct ValueOrExn<'a> {
    /* We re-use the representation of Value<'a>, even though the
     * (private) contents might not represent a valid element of
     * the `Value` type. */
    val_or_exn: Value<'a>,
}

extern "C" {
    fn caml_format_exception(v: Value) -> *mut c_char;
    fn caml_stat_free(cstr: *mut c_char);
}

impl<'a> ValueOrExn<'a> {
    /** `extract_exception` returns `Result::Ok(v)` if it denotes
     * the normal return of a value `v`, or `Result::Err(e)` if it
     * denotes that an exception value `e` has been raised. */
    pub fn extract_exception(&self) -> Result<Value<'a>, Value<'a>> {
        let raw = self.val_or_exn.as_raw();
        if raw & 3 == 2 {
            Result::Err(unsafe { Value::from_raw(raw & !3) })
        } else {
            Result::Ok(self.val_or_exn)
        }
    }

    pub fn format_exception(&self) -> Option<String> {
        match self.extract_exception() {
            Ok(_) => Option::None,
            Err(e) => unsafe {
                let c_err_string = caml_format_exception(e);
                let err_string = {
                    let bytes: &[u8] = CStr::from_ptr(c_err_string).to_bytes();
                    String::from_utf8_lossy(bytes).into_owned()
                };
                caml_stat_free(c_err_string);
                Option::Some(err_string)
            },
        }
    }

    pub fn expect(&self, msg: &str) -> Value<'a> {
        match self.extract_exception() {
            Ok(v) => v,
            Err(_) => {
                let error = self.format_exception().unwrap();
                panic!("{}: {}", msg, error)
            }
        }
    }

    pub fn unwrap(&self) -> Value<'a> {
        self.expect("called `ValueOrExn::unwrap()` on a raised exception")
    }
}
