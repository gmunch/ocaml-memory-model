/* Copyright (c) 2021-2023, Guillaume Munch-Maccagnoni
   SPDX-License-Identifier: LGPL-3.0-only WITH LGPL-3.0-linking-exception
*/

use core::sync::atomic::{AtomicUsize, Ordering};
use ocaml::{DomainLock, Value, ValueCell, ValueRef};
use ocaml_boxroot;
use std::ops::Deref;
use std::option::Option;

pub struct Boxroot {
    ptr: ocaml_boxroot::BoxRoot,
}

impl Boxroot {
    fn fail() {
        let msg = match (boxroot_status(), errno) {
            (Status::NotSetup, _) => "boxroot not setup",
            (Status::ToreDown, _) => "boxroot_teardown has already been called",
            (Status::Invalid, _) => "permanent boxroot failure (the hooks have been overwritten?)",
            // transient failures
            (Status::Running, ENOMEM) => "boxroot: out of memory",
            (Status::Running, EPERM) => "boxroot: domain lock not held",
            (_, _) => "boxroot: unknown transient failure",
        };
        panic!("{}", msg);
    }

    /** Roots an OCaml value
     *
     * This panics in case of an allocation failure (e.g. if the
     * domain lock is not held). */
    pub fn new<'a>(val: Value<'a>) -> Boxroot {
        unsafe {
            let ptr = boxroot_create(val).unwrap_or_else(fail);
            Boxroot { ptr }
        }
    }

    fn get_ref<'a>(&'a self) -> ValueRef<'a> {
        boxrootself.ptr.as_ref()
    }

    pub fn get<'a>(&self, gc: &'a DomainLock) -> Value<'a> {
        self.get_ref().as_val(gc)
    }

    pub fn keep<'a>(&'a mut self, val: Value) -> ValueRef<'a> {
        let ptr = &mut self.ptr as *mut BoxrootC;
        unsafe { boxroot_modify(ptr, val) };
        self.get_ref()
    }
}

impl Drop for Boxroot {
    fn drop(&mut self) {
        unsafe { boxroot_delete(self.ptr) }
    }
}

impl Deref for Boxroot {
    type Target = ValueCell;

    fn deref(&self) -> ValueRef {
        unsafe { self.ptr.as_ref() }
    }
}

/** Boxroot manages deletion from arbitrary threads, and accesses to
 * the contents are protected by a domain lock. One can, therefore,
 * send a boxroot to another thread, share it between threads via a
 * std::sync::Arc, use it as a payload for `panic_any` to re-raise an
 * OCaml exception as a panic, etc. */
unsafe impl Send for Boxroot {}
unsafe impl Sync for Boxroot {}
