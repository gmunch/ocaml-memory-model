/* Copyright (c) 2021-2023, Guillaume Munch-Maccagnoni
   SPDX-License-Identifier: LGPL-3.0-only WITH LGPL-3.0-linking-exception
*/

// FIXME
#![allow(dead_code)]
#![cfg_attr(feature = "no_std", no_std)]

pub mod atomic_value_ref;
pub mod boxroot;
pub mod domain_lock;
pub mod mut_value_ref;
pub mod value;
pub mod value_or_exn;
pub mod value_ref;
pub mod value_unshared;

pub use domain_lock::DomainLock;

/*
#[cfg(feature = "no_std")]
use core::ffi::{c_char, CStr}; //Requires Rust 1.64

#[cfg(not(feature = "no_std"))]
use std::{ffi::CStr, os::raw::{c_char}};
*/

/* Most of the functions in this crate do very simple operations which
 * would usually be inlined by the compiler. To enable cross-crate
 * inlining, we annotate every such public function with #[inline].
 */

// Just a test to verify that it compiles and links right
// Run with: cargo test --features "link-ocaml-runtime-and-dummy-program"
#[cfg(test)]
mod tests {}
