/* Copyright (c) 2021, 2022, Guillaume Munch-Maccagnoni
   SPDX-License-Identifier: LGPL-3.0-only WITH LGPL-3.0-linking-exception
*/

/* Reference types */

/** `ValueCell` is an OCaml value type with by-reference
 * semantics. Unlike Value<'a>, it is meant to be passed by
 * reference. The lifetime of the reference determines its
 * validity.
 *
 * A `ValueCell` can reside inside the OCaml heap and also allows
 * concurrent mutation by the GC, such that it can be used to
 * represent GC roots.
 *
 * Dereferencing a `ValueRef<'a>` requires passing the `DomainLock`
 * capability (unlike Value<'a>).
 */
#[repr(transparent)]
pub struct ValueCell {
    /* The atomic plays two roles:
     * - it can reside on the OCaml heap while satisfying the
     * OCaml memory model: reads must be relaxed atomic (a bit
     * stronger than that actually, but this is another
     * question),
     * - it behaves like UnsafeCell, letting the compiler know
     * that the contents can change at the whim of the GC. */
    atomic: AtomicIsize,
}

impl ValueCell {
    /** Create a trivial Cell (containing an immediate or a static
     * value), not tracked by the GC.  */
    pub fn new(val: Value<'static>) -> ValueCell {
        ValueCell {
            atomic: AtomicIsize::new(val.as_raw()),
        }
    }

    pub fn as_val<'a>(&self, _gc: &'a DomainLock) -> Value<'a> {
        unsafe { Value::from_raw(self.atomic.load(Ordering::Relaxed)) }
    }

    pub fn from_ref<'a>(val: &'a Value<'a>) -> ValueRef<'a> {
        unsafe { std::mem::transmute(val) }
    }
}

/** `ValueRef` = `&ValueCell` is a reference to a value which is
 * polymorphic in the way the validity of the value is ensured:
 * - `&'static ValueRef`: a static or immediate value
 * - `&'a ValueRef` where 'a is the lifetime of a borrow of the
 *   domain lock: an unrooted value, valid until the next GC.
 * - `&'a ValueRef` where 'a may outlive any borrow of the domain
 *   lock: a rooted value, which keeps the value alive and is
 *   updated whenever the value is moved by the GC.
 *
 * The type `ValueCell` does not specify how the latter is
 * achieved, so different rooting mechanisms give `ValueRef`s that
 * are interoperable.
 *
 * A `ValueRef` can only be accessed with a `DomainLock`
 * capability.
 */
pub type ValueRef<'a> = &'a ValueCell;

/** It is safe to send `ValueRefs` and other reference types
 * accross threads, for the same reason it is safe to do so for
 * references to `Value`s.
 */
unsafe impl Sync for ValueCell {}

/** `HeapCell` is a stronger version of `ValueCell`, that has
 * permission to access the heap like `Value`. Unlike a `ValueCell`,
 * references are always assumed to be invalidated by allocations. In
 * exchange, one does not need the `DomainLock` capability to access a
 * `&HeapCell` (the reference carries such a permission implicitly).
 *
 * For polymorphism purposes, it dereferences into a `ValueRef`. */
#[repr(transparent)]
pub struct HeapCell {
    cell: ValueCell,
}

/* TODO: how useful, if we deref into ValueRef? */
impl Deref for HeapCell {
    type Target = ValueCell;
    fn deref(&self) -> ValueRef {
        &self.cell
    }
}

impl HeapCell {
    pub fn as_val<'a>(&'a self) -> Value<'a> {
        unsafe {
            let gc = DomainLock::create_mut();
            self.cell.as_val(gc)
        }
    }
}
